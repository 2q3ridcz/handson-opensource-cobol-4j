       IDENTIFICATION          DIVISION.
       PROGRAM-ID.             OTAMESHI.
       ENVIRONMENT             DIVISION.
       CONFIGURATION           SECTION.
       INPUT-OUTPUT            SECTION.
       DATA                    DIVISION.
       FILE                    SECTION.
       WORKING-STORAGE         SECTION.
       01  MOJI                PIC X(15).
       LINKAGE                 SECTION.
       PROCEDURE               DIVISION.
           MOVE "Hello World!!" TO MOJI.
           DISPLAY "MOJI = " MOJI UPON CONSOLE.
       EXIT.
